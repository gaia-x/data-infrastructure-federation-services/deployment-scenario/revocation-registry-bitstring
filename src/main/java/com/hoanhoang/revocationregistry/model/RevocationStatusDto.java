package com.hoanhoang.revocationregistry.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RevocationStatusDto {
    private boolean initiate = false;
    private String credentialUrl;
    private String purpose;
    private String issuerId;
    private String reference;
    private int ttl;
    private List<CredentialSubject.RevocationMessage> statusMessages = new ArrayList<>();
    private int size;
    private boolean isReversible;
}

