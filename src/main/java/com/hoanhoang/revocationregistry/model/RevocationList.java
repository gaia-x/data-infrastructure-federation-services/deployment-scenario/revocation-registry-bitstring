package com.hoanhoang.revocationregistry.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class RevocationList {

    @JsonProperty("@context")
    private List<String> context = new ArrayList<>(List.of("https://www.w3.org/ns/credentials/v2"));
    private String id;
    private List<String> type = new ArrayList<>(List.of("VerifiableCredential", "BitstringStatusListCredential"));
    private String issuer;
    private String validFrom;
    private CredentialSubject credentialSubject;
}
