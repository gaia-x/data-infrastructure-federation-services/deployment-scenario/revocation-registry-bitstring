package com.hoanhoang.revocationregistry.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CredentialSubject {
    private String id;
    private String type;
    private long ttl;
    private String statusPurpose;
    private String reference;
    private long size;
    private String encodedList;

    private List<RevocationMessage> statusMessages;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class RevocationMessage{

        private String status;
        private String value;
    }
}
