package com.hoanhoang.revocationregistry.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RevocationStatusEntry {

    private String type;
    private String id;
    private String statusPurpose;
    private String statusListIndex;
    private String statusListCredential;
}
