package com.hoanhoang.revocationregistry.rest;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class RevocationConfigVerificationParameter {
    
    @NotNull
    private String purpose;

    @NotNull
    @JsonIgnore
    private String credentialUrl;

    @JsonIgnore
    private String issuerId;

}
