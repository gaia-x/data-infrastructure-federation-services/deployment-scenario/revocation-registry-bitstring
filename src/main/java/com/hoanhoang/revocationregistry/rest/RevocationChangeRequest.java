package com.hoanhoang.revocationregistry.rest;

import com.beust.klaxon.Json;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class RevocationChangeRequest {

    @JsonProperty("targetedStatus")
    @NotNull
    private String targetedStatus;

    @NotNull
    @JsonProperty("credentialStatus")
    private RevocationConfigParameter revocationConfigParameter;
}
