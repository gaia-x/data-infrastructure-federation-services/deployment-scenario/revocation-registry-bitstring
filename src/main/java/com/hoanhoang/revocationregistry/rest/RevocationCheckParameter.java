package com.hoanhoang.revocationregistry.rest;


import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RevocationCheckParameter {

    @NotNull
    private String type;

    @NotNull
    private String id;

    @NotNull
    private String statusPurpose;

    @NotNull
    private String statusListIndex;

    @NotNull
    private String statusListCredential;
}
