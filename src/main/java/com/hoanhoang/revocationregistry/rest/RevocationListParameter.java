package com.hoanhoang.revocationregistry.rest;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hoanhoang.revocationregistry.model.CredentialSubject;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RevocationListParameter {

    @NotNull
    private String purpose;

    @NotNull
    @JsonIgnore
    private String credentialUrl;

    @JsonIgnore
    private String issuerId;

    @NotNull
    private String reference;

    @NotNull
    private int ttl;

    @NotNull
    private List<CredentialSubject.RevocationMessage> statusMessages = new ArrayList<>();

    @NotNull
    private int size;

    @NotNull
    private boolean isReversible;
}
