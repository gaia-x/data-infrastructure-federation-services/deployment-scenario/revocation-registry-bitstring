package com.hoanhoang.revocationregistry.rest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RevocationConfigParameter {
    private String type;
    private String id;
    private String statusPurpose;
    private String statusListIndex;
    private String statusListCredential;
}
