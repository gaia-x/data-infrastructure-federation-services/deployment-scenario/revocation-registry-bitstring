package com.hoanhoang.revocationregistry.rest;


import com.beust.klaxon.Json;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RevocationStatusRequest {

//    @JsonIgnore
//    private String credentialUrl;

    @JsonProperty("purpose")
    @NotNull
    private String purpose;
}
