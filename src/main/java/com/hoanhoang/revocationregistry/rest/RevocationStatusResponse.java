package com.hoanhoang.revocationregistry.rest;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RevocationStatusResponse {

    private String status;
    private String value;
}
