package com.hoanhoang.revocationregistry.vc.revocation.service;


import com.hoanhoang.revocationregistry.enums.RevocationType;
import com.hoanhoang.revocationregistry.model.RevocationList;
import com.hoanhoang.revocationregistry.model.RevocationStatusDto;
import com.hoanhoang.revocationregistry.model.RevocationStatusEntry;
import com.hoanhoang.revocationregistry.rest.RevocationCheckParameter;
import com.hoanhoang.revocationregistry.rest.RevocationConfigParameter;
import com.hoanhoang.revocationregistry.rest.RevocationConfigVerificationParameter;
import com.hoanhoang.revocationregistry.rest.RevocationListParameter;
import com.hoanhoang.revocationregistry.rest.RevocationStatusResponse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class OAuthStatusListRevocationProvider extends RevocationProvider {

    private RevocationType revocationType = RevocationType.OAUTH_STATUS_LIST;

    @Override
    public RevocationType getRevocationType() {
        return revocationType;
    }

    @Override
    public void transitionRevocationStatus(String targetedStatus, RevocationConfigParameter parameter) {

    }

    @Override
    public List<RevocationStatusEntry> getRevocationStatusEntry(RevocationStatusDto revocationStatusDto) {
        return null;
    }

    @Override
    public void configureRevocationList(RevocationListParameter parameter) {

    }

    @Override
    public RevocationStatusResponse verifyRevocationStatus(RevocationCheckParameter parameter) {
        return null;
    }

    @Override
    public List<Map<String, Object>> getRevocationListByIssuer(String issuerId) {
        return null;
    }

    @Override
    public List<Map<String, Object>> getAllRevocationList() {
        return null;
    }

    @Override
    public boolean checkRevocationListConfiguration(RevocationConfigVerificationParameter parameter) {
        return false;
    }
}
