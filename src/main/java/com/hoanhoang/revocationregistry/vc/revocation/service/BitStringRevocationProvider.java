package com.hoanhoang.revocationregistry.vc.revocation.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hoanhoang.revocationregistry.common.CommonUtils;
import com.hoanhoang.revocationregistry.enums.RevocationType;
import com.hoanhoang.revocationregistry.model.RevocationStatusDto;
import com.hoanhoang.revocationregistry.model.RevocationStatusEntry;
import com.hoanhoang.revocationregistry.rest.RevocationCheckParameter;
import com.hoanhoang.revocationregistry.rest.RevocationConfigParameter;
import com.hoanhoang.revocationregistry.rest.RevocationConfigVerificationParameter;
import com.hoanhoang.revocationregistry.rest.RevocationListParameter;
import com.hoanhoang.revocationregistry.rest.RevocationStatusResponse;
import com.hoanhoang.revocationregistry.vc.revocation.list.RevocationListHelper;
import com.hoanhoang.revocationregistry.vc.revocation.list.RevocationListServiceManager;
import com.hoanhoang.revocationregistry.vc.storage.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class BitStringRevocationProvider extends RevocationProvider {


    private RevocationType revocationType = RevocationType.BIT_STRING;
    private final RevocationListServiceManager revocationListService;
    private final StorageService storageService;
    private final ModelMapper modelMapper;
    private final ObjectMapper objectMapper;

    public BitStringRevocationProvider(RevocationListServiceManager revocationListService,
                                       StorageService storageService,
                                       ModelMapper modelMapper,
                                       ObjectMapper objectMapper){
        this.revocationListService = revocationListService;
        this.storageService = storageService;
        this.modelMapper = modelMapper;
        this.objectMapper = objectMapper;
    }
    @Override
    public RevocationType getRevocationType() {
        return revocationType;
    }

    @Override
    public List<RevocationStatusEntry> getRevocationStatusEntry(RevocationStatusDto revocationStatusDto) {

        RevocationStatusEntry entry = revocationListService.createOrUpdateRevocationList(this.revocationType, revocationStatusDto);

        return List.of(entry);
    }

    @Override
    public RevocationStatusResponse verifyRevocationStatus(RevocationCheckParameter checkParameter) {
        RevocationConfigParameter configParameter = new RevocationConfigParameter();
        configParameter.setId(checkParameter.getId());
        configParameter.setType(checkParameter.getType());
        configParameter.setStatusPurpose(checkParameter.getStatusPurpose());
        configParameter.setStatusListIndex(checkParameter.getStatusListIndex());
        configParameter.setStatusListCredential(checkParameter.getStatusListCredential());

        RevocationStatusResponse response  = revocationListService.verifyCredentialStatus(configParameter);

        return response;
    }

    @Override
    public void configureRevocationList(RevocationListParameter parameter) {
        // Check if there is already a revocation list for this parameter
        String revocationListFilename = RevocationListHelper.createRevocationListFilename(revocationType, parameter.getPurpose(), parameter.getIssuerId());
        if(storageService.checkIfRevocationListExists(revocationListFilename)){
            log.error("There is already a revocation list for the purpose {} of the issuer: {}", parameter.getPurpose(), parameter.getIssuerId());

            throw new ResponseStatusException(HttpStatus.CONFLICT,
                    String.format("There is already a revocation list for the purpose %s of the issuer: %s", parameter.getPurpose(), parameter.getIssuerId()));
        }


        // Check if the provided parameters are correct
        Set<String> revocationMessages = parameter.getStatusMessages().stream()
                .map(m -> m.getStatus())
                .collect(Collectors.toSet());

        Set<String> expectedMessages = CommonUtils.generateBinaryStrings(parameter.getSize());


        if(! expectedMessages.containsAll(revocationMessages) ||
                (expectedMessages.size() != revocationMessages.size())){
            log.error("Failed to configure the revocation list due to revocation mismatch. \n Received: {} and \n Expected: {} ",
                    Arrays.toString(revocationMessages.toArray()), Arrays.toString(expectedMessages.toArray()));

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Failed to configure the revocation list due to revocation mismatch. \n Received: %s and \n Expected: %s ",
                            Arrays.toString(revocationMessages.toArray()), Arrays.toString(expectedMessages.toArray())));
        }

        // Create a new revocation list with the configured parameters by issuing the first revocation status
        RevocationStatusDto statusDto = modelMapper.map(parameter, RevocationStatusDto.class);
        statusDto.setInitiate(true);
        getRevocationStatusEntry(statusDto);

    }

    @Override
    public void transitionRevocationStatus(String targetedStatus, RevocationConfigParameter parameter) {

        revocationListService.updateStatusCredentialRecord(targetedStatus, parameter);
    }

    @Override
    public List<Map<String, Object>> getRevocationListByIssuer(String issuerId) {
        String type = revocationType.getType();
        List<String> revocationListFilenames = storageService.getAllRevocationListFilenamesForIssuer(issuerId, type);
        List<Map<String, Object>> revocationList = getRevocationListByFilenames(revocationListFilenames);

        return revocationList;
    }

    @Override
    public List<Map<String, Object>> getAllRevocationList() {
        String type = revocationType.getType();
        List<String> revocationListFilenames = storageService.getAllRevocationListFilenames(type);
        List<Map<String, Object>> revocationList = getRevocationListByFilenames(revocationListFilenames);

        return revocationList;
    }

    @Override
    public boolean checkRevocationListConfiguration(RevocationConfigVerificationParameter parameter) {
        String revocationListFilename = RevocationListHelper.createRevocationListFilename(revocationType, parameter.getPurpose(), parameter.getIssuerId());
        return storageService.checkIfRevocationListExists(revocationListFilename);
    }

    private List<Map<String, Object>> getRevocationListByFilenames(List<String> revocationListFilenames ){
        List<String> revocationLists = revocationListFilenames.stream()
                .map(list -> storageService.getRevocationList(list))
                .collect(Collectors.toList());

        List<Map<String, Object>> res = revocationLists.stream()
                .map(list -> convertRevocationListToJSON(list))
                .collect(Collectors.toList());

        return res;
    }

    private Map<String, Object> convertRevocationListToJSON(String revocationList){
        TypeReference<Map<String, Object>> typeReference = new TypeReference<Map<String, Object>>() {};
        Map<String, Object> revocationListJSON = null;
        try {
            revocationListJSON = objectMapper.readValue(revocationList, typeReference);
        } catch (JsonProcessingException e) {
            log.error("Failed to convert revocation list to JSON");
            throw new RuntimeException(e);
        }
        return revocationListJSON;
    }
}
