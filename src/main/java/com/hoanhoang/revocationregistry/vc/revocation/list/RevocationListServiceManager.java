package com.hoanhoang.revocationregistry.vc.revocation.list;

import com.hoanhoang.revocationregistry.enums.RevocationType;
import com.hoanhoang.revocationregistry.model.RevocationStatusDto;
import com.hoanhoang.revocationregistry.model.RevocationStatusEntry;
import com.hoanhoang.revocationregistry.rest.RevocationConfigParameter;
import com.hoanhoang.revocationregistry.rest.RevocationStatusResponse;
import id.walt.credentials.w3c.VerifiableCredential;

import java.util.BitSet;

public interface RevocationListServiceManager {

    String getDIDWebDetails();
    RevocationStatusEntry createOrUpdateRevocationList(RevocationType revocationType, RevocationStatusDto statusDto);
    //VerifiableCredential createRevocationListTemplate(RevocationType revocationType, RevocationStatusDto statusDto, CredentialSubject credentialSubject);
    String signRevocationList(String subjectId, String credentialId, VerifiableCredential toBeSignedVC);

    RevocationStatusEntry buildRevocationStatusEntry(String revocationListId, StatusListIndex index, String purpose);
    RevocationStatusResponse verifyCredentialStatus(RevocationConfigParameter config);
    void updateStatusCredentialRecord(String status, RevocationConfigParameter parameter);
    String createEncodedBitString(BitSet bitSet);

}
