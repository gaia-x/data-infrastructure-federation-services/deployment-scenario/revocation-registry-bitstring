package com.hoanhoang.revocationregistry.vc.revocation.list;

import com.hoanhoang.revocationregistry.config.Constants;
import com.hoanhoang.revocationregistry.enums.RevocationType;
import com.hoanhoang.revocationregistry.model.RevocationStatusDto;
import com.hoanhoang.revocationregistry.model.RevocationStatusEntry;
import com.hoanhoang.revocationregistry.rest.RevocationConfigParameter;
import com.hoanhoang.revocationregistry.rest.RevocationStatusRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.BitSet;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


@Slf4j
public class RevocationListHelper {
    public static String createRelativeRevocationListUrl(RevocationType revocationType, String revocationPurpose, String issuerId){

        // https://revocation-service.com/issuerId-revocation-bitstring.json/
        String postfix = revocationType.getType();
        String result =  issuerId+ "-" + revocationPurpose + "-" + postfix;
        log.debug("Create a filename {} for revocation list of issuer {}", result, issuerId);
        return result;
    }

    public static String createRevocationListFilename(RevocationType revocationType, String revocationPurpose, String issuerId){
        // Format: issuerId-revocation-bitstring.json
        String postfix = revocationType.getType() + ".txt";
        String result = issuerId + "-" + revocationPurpose + "-" + postfix;
        log.debug("Create a filename {} for a revocation list of issuer {}  ", result, issuerId);
        return result;
    }

    public static String createIndexFilename(RevocationType revocationType, String revocationPurpose, String issuerId){
        // Format: issuerId-revocation-bitstring-index.json
        String postfix = revocationType.getType() +  "-index" + ".json";
        String result =  issuerId+ "-" + revocationPurpose + "-" + postfix;
        log.debug("Create a filename {} for index of issuer {}", result, issuerId);
        return result;
    }

    public static String resolveRevocationListFilenameFromRevocationStatus(RevocationConfigParameter parameter){
        String statusListCredential = parameter.getStatusListCredential();
        String revocationListFilename = StringUtils.substringAfterLast(statusListCredential, "/") + ".txt";
        log.info("Resolving the revocation list file name: {}", revocationListFilename);

        return  revocationListFilename;
    }


    public static String resolveIndexFilenameFromRevocationStatus(RevocationConfigParameter parameter){
        String statusListCredential = parameter.getStatusListCredential();
        String indexFilename = StringUtils.substringAfterLast(statusListCredential, "/") + "-index.json";


        log.info("Resolving the index filename {}", indexFilename);
        return indexFilename;
    }

    public static String createIdForRevocationEntry(RevocationStatusDto revocationStatusDto, int index){
        String id = revocationStatusDto.getCredentialUrl() + "#" + String.valueOf(index);
        log.debug("Creating an ID {} for a Revocation Status Entry ", id);
        return id;
    }

    public static String createIdForRevocationList(RevocationStatusDto statusDto){
        String id = statusDto.getCredentialUrl();
        log.debug("Creating an ID {} for a Revocation List ", id);
        return id;
    }

    public static byte[] uncompressGzip(byte[] data) {
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(data);
             GZIPInputStream gzipInputStream = new GZIPInputStream(byteArrayInputStream);
             ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()
        ) {
            byte[] buffer = new byte[1024];
            int length;
            while ((length = gzipInputStream.read(buffer)) > 0) {
                byteArrayOutputStream.write(buffer, 0, length);
            }

            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            log.error("Failed to uncompressGZIP");
            throw new RuntimeException(e);
        }
    }

    public static byte[] compressGzip(byte[] data) {
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
        ) {
            gzipOutputStream.write(data);
            gzipOutputStream.finish();
            return byteArrayOutputStream.toByteArray();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] bitSetToByteArray(BitSet bitSet) {
        System.out.println(String.format("Bitset size: %d and length: %d", bitSet.size(), bitSet.length()));
        int lastIndex = 0;
        int currentIndex = bitSet.nextSetBit(lastIndex);
        StringBuilder builder = new StringBuilder();

        while (currentIndex > -1) {
            int delta = 1 % (lastIndex + 1);
            for (int i = 0; i < currentIndex - lastIndex - delta; i++) {
                builder.append(0);
            }
            builder.append("1");
            lastIndex = currentIndex;
            currentIndex = bitSet.nextSetBit(lastIndex + 1);
        }
        System.out.println(String.format("BitSet Size:  %d and LastIndex: %d", bitSet.size(), lastIndex));
        for (int i = 0; i < bitSet.size() - lastIndex - 1; i++) {
            builder.append("0");
        }
        return builder.toString().getBytes();
        //return builder.toString().getBytes();
    }

    public static BitSet toBitSet(String data) {
        BitSet bitSet = new BitSet(Constants.BIT_STRING_LENGTH);
        char[] charArray = data.toCharArray();

        for (int i = 0; i < charArray.length; i++) {
            if (charArray[i] == '1') {
                bitSet.set(i);
            }
        }
        return bitSet;
    }
}
