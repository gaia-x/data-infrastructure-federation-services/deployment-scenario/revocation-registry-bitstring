package com.hoanhoang.revocationregistry.vc.revocation.service;


import com.hoanhoang.revocationregistry.enums.RevocationType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RevocationManager {

    private final OAuthStatusListRevocationProvider oAuthStatusListRevocationProvider;
    private final BitStringRevocationProvider bitStringRevocationProvider;


    public RevocationProvider getRevocationProvider(RevocationType revocationType){
        RevocationProvider revocationProvider = switch (revocationType){
            case BIT_STRING -> bitStringRevocationProvider;
            case OAUTH_STATUS_LIST -> oAuthStatusListRevocationProvider;

        };

        return revocationProvider;
    }
}
