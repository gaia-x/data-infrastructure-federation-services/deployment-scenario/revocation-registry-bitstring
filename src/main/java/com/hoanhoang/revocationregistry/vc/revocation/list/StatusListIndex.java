package com.hoanhoang.revocationregistry.vc.revocation.list;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StatusListIndex {

    private int index;
    private int size;
    private boolean isReversible;
}
