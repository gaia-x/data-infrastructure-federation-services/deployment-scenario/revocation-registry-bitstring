package com.hoanhoang.revocationregistry.vc.revocation.list;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hoanhoang.revocationregistry.config.Constants;
import com.hoanhoang.revocationregistry.enums.RevocationType;
import com.hoanhoang.revocationregistry.model.CredentialSubject;
import com.hoanhoang.revocationregistry.model.RevocationStatusDto;
import com.hoanhoang.revocationregistry.model.RevocationStatusEntry;
import com.hoanhoang.revocationregistry.rest.RevocationConfigParameter;
import com.hoanhoang.revocationregistry.rest.RevocationStatusResponse;
import com.hoanhoang.revocationregistry.vc.storage.StorageService;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.credentials.w3c.W3CContext;
import id.walt.credentials.w3c.builder.W3CCredentialBuilder;
import id.walt.credentials.w3c.templates.VcTemplate;
import id.walt.credentials.w3c.templates.VcTemplateService;
import id.walt.crypto.KeyAlgorithm;
import id.walt.crypto.KeyId;
import id.walt.model.Did;
import id.walt.model.DidMethod;
import id.walt.servicematrix.ServiceMatrix;
import id.walt.services.did.DidService;
import id.walt.services.did.DidWebCreateOptions;
import id.walt.services.key.KeyService;
import id.walt.signatory.Ecosystem;
import id.walt.signatory.ProofConfig;
import id.walt.signatory.ProofType;
import id.walt.signatory.Signatory;
import id.walt.signatory.dataproviders.MergingDataProvider;
import jakarta.annotation.PostConstruct;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.stream.Collectors;


@Service
@Slf4j
@Getter
@Setter
public class RevocationListService implements RevocationListServiceManager {

    @Value("${registry.did.base.domain}")
    private String didWebBaseDomain;

    private String currentDID;
    private StorageService storageService;
    private Signatory signatory;
    private ObjectMapper objectMapper;


    public RevocationListService(StorageService storageService, ObjectMapper objectMapper) {
        this.storageService = storageService;
        this.objectMapper = objectMapper;
    }

    @PostConstruct
    private void initialize(){
        new ServiceMatrix("service-matrix.properties");
        this.signatory = Signatory.Companion.getService();
        KeyService keyService = KeyService.Companion.getService();
        KeyId keyId = keyService.generate(KeyAlgorithm.EdDSA_Ed25519);

        // DID KEY
//        String currentDid = DidService.INSTANCE.create(DidMethod.key, keyId.getId(), null);
//        this.currentDID = currentDid;
//        log.info("Current DID of Revocation List Provider is:  {}", this.currentDID);

        // DID WEB:  Test purpose
        List<String> dids = DidService.INSTANCE.listDids();
        log.info("All DIDs in the system are {}", Arrays.toString(dids.toArray()));
        Optional<String> didString =  dids.stream().filter(did ->
                        did.contains(didWebBaseDomain)).findFirst();

        if(didString.isPresent()){
            log.warn("There is already a DID WEB for localhost with details: {}", DidService.INSTANCE.load(didString.get()).encode());
            this.currentDID = didString.get();
        } else {
            String currentDidWeb = DidService.INSTANCE.create(DidMethod.web,
                    keyId.getId(),
                    new DidWebCreateOptions(didWebBaseDomain, null));
            this.currentDID = currentDidWeb;

            Did did = DidService.INSTANCE.load(this.currentDID);
            log.info("Current DID: {} has details: {}", currentDidWeb, did.encode());
        }
    }

    @Override
    public String getDIDWebDetails() {
        String currentDidWebObject = DidService.INSTANCE.load(this.currentDID).encode();

        return currentDidWebObject;
    }

    @Override
    public RevocationStatusEntry createOrUpdateRevocationList(RevocationType revocationType, RevocationStatusDto statusDto) {

        String revocationList = statusDto.getCredentialUrl();
        String revocationListPath = StringUtils.substringAfterLast(revocationList, "/") + ".json";
        log.info("Resolving the status list with name {}", revocationListPath);

        String revocationListID = RevocationListHelper.createIdForRevocationList(statusDto);
        if (storageService.checkIfRevocationListExists(//statusDto.getCredentialUrl()
                RevocationListHelper.createRevocationListFilename(revocationType, statusDto.getPurpose(), statusDto.getIssuerId())
        )) {

            String indexFilename = RevocationListHelper.createIndexFilename(revocationType, statusDto.getPurpose(), statusDto.getIssuerId());
            StatusListIndex index = storageService.getIndexFile(indexFilename);
            StatusListIndex updatedIndex = new StatusListIndex(index.getIndex() + index.getSize(), index.getSize(), index.isReversible());
            storageService.storeIndexFile(indexFilename, updatedIndex);
            RevocationStatusEntry entry = buildRevocationStatusEntry(revocationListID, updatedIndex, statusDto.getPurpose());

            return entry;
        } else if(! statusDto.isInitiate()){
            // This is not a revocation init. And there is no revocation exists:
            log.error("Cannot request a revocation status due to a missing revocation list of purpose: {} for issuer: {}", statusDto.getPurpose(), statusDto.getIssuerId());
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Cannot request a revocation status due to a missing revocation list of purpose: %s for issuer: %s. Try to configure one", statusDto.getPurpose(), statusDto.getIssuerId()));
        } else {

            // Initiate a credential subject for the revocation list
            CredentialSubject credentialSubject = new CredentialSubject();
            //https://example.com/status/3#list
            credentialSubject.setId(revocationListID + "#list");
            credentialSubject.setType("BitstringStatusList");
            credentialSubject.setTtl(statusDto.getTtl());

            //credentialSubject.setSize(2);
            credentialSubject.setSize(statusDto.getSize());

            //credentialSubject.setStatusPurpose("revocation");
            credentialSubject.setStatusPurpose(statusDto.getPurpose());

            //credentialSubject.setReference("https://myreference.com");
            credentialSubject.setReference(statusDto.getReference());

            String encodedList = createEncodedBitString(new BitSet(Constants.BIT_STRING_LENGTH));
            credentialSubject.setEncodedList(encodedList);

            credentialSubject.setStatusMessages(statusDto.getStatusMessages());


            VerifiableCredential revocationListTemplate = createRevocationListTemplate(revocationType, statusDto, credentialSubject);
            // Set revocation list ID: https://example.com/credentials/status/3
            revocationListTemplate.setId(revocationListID);
            revocationListTemplate.setContext(List.of(new W3CContext(Constants.W3C_VERIFIABLE_CREDENTIAL_CONTEXT)));
            revocationListTemplate.setType(Constants.BIT_STRING_VC_TYPE);

            String signedVC = signRevocationList(credentialSubject.getId(), revocationListID, revocationListTemplate);

            StatusListIndex statusListIndex = new StatusListIndex(0, statusDto.getSize(), statusDto.isReversible());

            String indexFileName = RevocationListHelper.createIndexFilename(revocationType, statusDto.getPurpose(), statusDto.getIssuerId());
            storageService.storeIndexFile(indexFileName, statusListIndex);

            String revocationListFilename = RevocationListHelper.createRevocationListFilename(revocationType, statusDto.getPurpose(), statusDto.getIssuerId());
            storageService.storeRevocationList(revocationListFilename, signedVC);

            RevocationStatusEntry entry = buildRevocationStatusEntry(revocationListID, statusListIndex, statusDto.getPurpose());

            return entry;
        }
    }

    @Override
    public String signRevocationList(String subjectId, String credentialId, VerifiableCredential toBeSignedVC) {

        if(toBeSignedVC.getProof() != null){
            toBeSignedVC.setProof(null);
        }

        String signedVC = signatory.issue(W3CCredentialBuilder.Companion.fromPartial(toBeSignedVC),
                createProofConfig(currentDID, subjectId, credentialId,ProofType.LD_PROOF, Instant.now().plus(30, ChronoUnit.DAYS)),
                null,
                false
        );

//        boolean res = Auditor.Companion.getService().verify(signedVC, List.of(new SignaturePolicy())).getResult();
//        log.info("RESULT:  {}", res);
        log.info("The SIGNED Revocation List: {}", signedVC);
        return signedVC;
    }


    private VerifiableCredential createRevocationListTemplate(RevocationType revocationType,
                                                             RevocationStatusDto statusDto,
                                                             CredentialSubject credentialSubject) {

        VerifiableCredential verifiableCredential = switch (revocationType) {
            case BIT_STRING -> {
                // Load the template of BitString
                VcTemplate vcTemplate = VcTemplateService.Companion.getService()
                        .getTemplate("StatusList2021Credential", true, "");

                //Map<String, Object> data = Map.ofEntries(credentialSubjectEntry(statusDto.getVcData()));
                Map<String, Object> subjectData = objectMapper.convertValue(credentialSubject, new TypeReference<Map<String, Object>>() {});
                Map<String, Map<String, Object>> data = Map.ofEntries(credentialSubjectEntry(subjectData));

                VerifiableCredential vc = mergingCustomData(data);
                String revocationListID = StringUtils.substringBeforeLast(credentialSubject.getId(), "#");
                log.info("Get the revocation ListId: {} from subjectId: {}", revocationListID, credentialSubject.getId());
                vc.setId(revocationListID);
                log.info("Get VC details: {}", vc.encode());
                yield (vc);
            }
            case OAUTH_STATUS_LIST -> {

                yield(null);
            }
        };
        return verifiableCredential;
    }

    @Override
    public RevocationStatusEntry buildRevocationStatusEntry(String revocationListId, StatusListIndex index, String purpose) {
        RevocationStatusEntry entry = RevocationStatusEntry.builder()
                .id(revocationListId + "#" + String.valueOf(index.getIndex()))
                .type("BitstringStatusListEntry")
                .statusPurpose(purpose)
                .statusListIndex(String.valueOf(index.getIndex()))
                .statusListCredential(revocationListId)
                .build();
        return entry;
    }

    @Override
    public void updateStatusCredentialRecord(String status, RevocationConfigParameter parameter) {
        //Check if the associated revocation list exists
        String revocationListFilename = RevocationListHelper.resolveRevocationListFilenameFromRevocationStatus(parameter);
        if(!storageService.checkIfRevocationListExists(revocationListFilename)){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, "There is no revocation list associated to the request");
        }

        //Load Index File and Revocation File
        String statusIndexFilename = RevocationListHelper.resolveIndexFilenameFromRevocationStatus(parameter);
        StatusListIndex statusListIndex = storageService.getIndexFile(statusIndexFilename);

        //Update the revocation list
        String revocationList = storageService.getRevocationList(revocationListFilename);
        VerifiableCredential revocationListVC = VerifiableCredential.Companion.fromString(revocationList);

        List<CredentialSubject.RevocationMessage> messages = getRevocationMessageFromRevocationList(revocationListVC);

        // Check if the targeted status is supported
        List<String> statues = messages.stream()
                .map(m -> m.getStatus())
                .collect(Collectors.toList());

        if(! statues.contains(status)){
            log.error("The specified status is not supported with the corresponding Revocation List. Try with the followings: {}", Arrays.toString(statues.toArray()));
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("The specified status is not supported with the corresponding Revocation List. Try with the followings: %s", Arrays.toString(statues.toArray())));
        }
        // Check if the revocation list support reversible

        String encodedList  = (String)revocationListVC.getCredentialSubject()
                .getProperties()
                .get("encodedList");

        log.info("Current encoded LIST:   {}", encodedList);
        BitSet bitSet = RevocationListHelper.toBitSet(
                new String(RevocationListHelper.uncompressGzip(Base64.getDecoder().decode(encodedList)))
        );

        int targetedStatusIndex = Integer.parseInt(status.replace("0x", ""), 2);
        boolean isReversible = statusListIndex.isReversible();

        int currentIndexStart = Integer.parseInt(parameter.getStatusListIndex());
        int currentStatusIndex;
        int i = 0;
        StringBuilder indexBuilder = new StringBuilder();
        for(i = currentIndexStart; i < currentIndexStart + statusListIndex.getSize(); i++){
            if(bitSet.get(i)){
                indexBuilder.append("1");
            } else if(! bitSet.get(i)){
                indexBuilder.append("0");
            }
        }

        currentStatusIndex = Integer.parseInt(indexBuilder.toString(), 2);
        if(!isReversible && (currentStatusIndex >= targetedStatusIndex)){
            log.error("The corresponding revocation list does not support status reversion. Try with another status");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The corresponding revocation list does not support status reversion");
        }


        // Do the status transition
        String messageIndexInBinaryString = status.replace("0x","");
        log.info("Representing the chosen status {} by string: {}", status, messageIndexInBinaryString);
        for( i = currentIndexStart; i < currentIndexStart + statusListIndex.getSize(); i++){
            if(messageIndexInBinaryString.charAt(i -currentIndexStart) == '0'){
                log.info("Index {} is cleared", i);
                bitSet.clear(i);
            } else if(messageIndexInBinaryString.charAt(i -currentIndexStart) == '1'){
                log.info("Index {} is set", i);
                bitSet.set(i);
            } else {
                log.info("The current character is {}", messageIndexInBinaryString.charAt(i -currentIndexStart));
            }
        }


        String updatedEncodedList = createEncodedBitString(bitSet);
        log.info("Updated encoded list: {}", updatedEncodedList);
        revocationListVC.getCredentialSubject().getProperties().put("encodedList", updatedEncodedList);
        String updatedRevocationList = signRevocationList(revocationListVC.getSubjectId(), revocationListVC.getId(), revocationListVC);
        storageService.storeRevocationList(revocationListFilename, updatedRevocationList);

    }

    @Override
    public String createEncodedBitString(BitSet bitSet) {
        String encodedList = Base64.getEncoder().encodeToString(
                RevocationListHelper.compressGzip(RevocationListHelper.bitSetToByteArray(bitSet))
        );
        log.debug("Initiating an encoded list: {}", encodedList);
        return encodedList;
    }

    @Override
    public RevocationStatusResponse verifyCredentialStatus(RevocationConfigParameter config) {

        int index = Integer.parseInt(config.getStatusListIndex());

        //Check if the associated revocation list exists
        String revocationListFilename = RevocationListHelper.resolveRevocationListFilenameFromRevocationStatus(config);
        if(!storageService.checkIfRevocationListExists(revocationListFilename)){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, "There is no revocation list associated to the request");
        }
        //Load Index File and Revocation File
        String statusIndexFilename = RevocationListHelper.resolveIndexFilenameFromRevocationStatus(config);
        StatusListIndex statusListIndex = storageService.getIndexFile(statusIndexFilename);

        //Load Revocation List
        String revocationList = storageService.getRevocationList(revocationListFilename);
        VerifiableCredential revocationListVC = VerifiableCredential.Companion.fromString(revocationList);

        List<CredentialSubject.RevocationMessage> revocationMessages = getRevocationMessageFromRevocationList(revocationListVC);

        String encodedList  = (String) revocationListVC.getCredentialSubject().getProperties().get("encodedList");
        BitSet bitSet = RevocationListHelper.toBitSet(new String(RevocationListHelper.uncompressGzip(
                Base64.getDecoder().decode(encodedList))));

        StringBuilder stringBuilder = new StringBuilder();

        for(int i = index; i < index + statusListIndex.getSize(); i++){
            if(bitSet.get(i)){
                log.info("The index at {} is 1", i);
                stringBuilder.append("1");
            } else {
                log.info("The index at {} is 0", i);
                stringBuilder.append("0");
            }
        }

        String key = stringBuilder.toString();

        log.info("The current status in String is: {}", key);
        int decimalKey = Integer.parseInt(key, 2);

        CredentialSubject.RevocationMessage message = revocationMessages.get(decimalKey);
        RevocationStatusResponse response = new RevocationStatusResponse(message.getStatus(), message.getValue());
        return response;
    }

    private List<CredentialSubject.RevocationMessage> getRevocationMessageFromRevocationList(VerifiableCredential revocationListVC){

        List<CredentialSubject.RevocationMessage> revocationMessages= new ArrayList<>();
        try {
            String messages = objectMapper.writeValueAsString(revocationListVC.getCredentialSubject()
                    .getProperties()
                    .get("statusMessages"));
            TypeReference<List<CredentialSubject.RevocationMessage>> typeReference = new TypeReference<List<CredentialSubject.RevocationMessage>>() {};
            revocationMessages = objectMapper.readValue(messages, typeReference);

            return revocationMessages;
        } catch (JsonProcessingException e) {
            log.error("Failed to read status Messages");
            throw new RuntimeException(e);
        }
    }

    private VerifiableCredential mergingCustomData(Map<String, Map<String, Object>> data) {
        VerifiableCredential verifiableCredential = new MergingDataProvider(data).populate(
                new W3CCredentialBuilder(),
                new ProofConfig(currentDID, currentDID, null, null, ProofType.LD_PROOF, null, null,
                        null, null, null, null, null, null, null, null, Ecosystem.DEFAULT,
                        null, "", "")
        ).build();

        return verifiableCredential;
    }

    private SimpleEntry<String, Map<String, Object>> credentialSubjectEntry(Map<String, Object> vcData) {
        SimpleEntry<String, String>[] pairs = vcData.entrySet().stream()
                .map(entry -> new SimpleEntry<String, Object>(entry.getKey(), entry.getValue()))
                .toArray(SimpleEntry[]::new);

        return new SimpleEntry<>("credentialSubject", Map.ofEntries(pairs));
    }

    private ProofConfig createProofConfig(String issuerDid, String subjectDid, String credentialId,  ProofType proofType, Instant expiration) {
        return new ProofConfig(issuerDid = issuerDid, subjectDid = subjectDid, null, null, proofType, null, null,
                null, credentialId, null, null, expiration, null, null, null, Ecosystem.DEFAULT,
                null, "", "");
    }

}
