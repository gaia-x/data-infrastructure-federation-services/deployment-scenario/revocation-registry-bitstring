package com.hoanhoang.revocationregistry.vc.revocation.service;

import com.hoanhoang.revocationregistry.enums.RevocationType;
import com.hoanhoang.revocationregistry.model.RevocationStatusDto;
import com.hoanhoang.revocationregistry.rest.*;
import com.hoanhoang.revocationregistry.model.RevocationStatusEntry;

import java.util.List;
import java.util.Map;

public abstract class RevocationProvider {
    public abstract RevocationType getRevocationType();
    public abstract List<RevocationStatusEntry> getRevocationStatusEntry(RevocationStatusDto revocationStatusDto);
    public abstract RevocationStatusResponse verifyRevocationStatus(RevocationCheckParameter parameter);
    public abstract List<Map<String, Object>> getRevocationListByIssuer(String issuerId);
    public abstract List<Map<String, Object>> getAllRevocationList();
    public abstract void transitionRevocationStatus(String targetedStatus, RevocationConfigParameter parameter);
    public abstract void configureRevocationList(RevocationListParameter parameter);
    public abstract boolean checkRevocationListConfiguration(RevocationConfigVerificationParameter parameter);
}
