package com.hoanhoang.revocationregistry.vc.storage;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.hoanhoang.revocationregistry.model.RevocationList;
import com.hoanhoang.revocationregistry.vc.revocation.list.StatusListIndex;
import id.walt.credentials.w3c.VerifiableCredential;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class StorageServiceProvider implements StorageService{


    @Value("${registry.storage.system.location}")
    private String rootPath;
    private Path location;
    private ObjectMapper objectMapper;

    public StorageServiceProvider(ObjectMapper objectMapper){
        this.objectMapper = objectMapper;
    }


    @PostConstruct
    private void initRevocationListStorageSystem(){
        location = Paths.get(".").resolve(rootPath).toAbsolutePath().normalize();
        try {
            log.info("Creating the root storage directory for revocation lists at {}", location);
            Files.createDirectories(location);
        } catch (IOException e) {
            log.error("Failed to create the root storage directory at {} due to {}", location, e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean checkIfRevocationListExists(String revocationList) {
        Path revocationListUrl = location.resolve(revocationList).normalize();
        log.info("Resolving the revocation list url at {}", revocationListUrl);

        try {
            Resource resource = new UrlResource(revocationListUrl.toUri());
            return resource.exists();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void storeIndexFile(String filename, StatusListIndex index) {
        Path filePath = location.resolve(filename).normalize();
        try {
            objectMapper.writeValue(filePath.toFile(), index);
        } catch (IOException e) {
            log.error("Failed to write index file {} due to error {}", filename, e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public void storeRevocationList(String filename, String revocationList) {
        Path filePath = location.resolve(filename).normalize();

        try {
            Files.write(filePath, revocationList.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            log.error("Failed to write the revocation list into the file: {}", filename);
            throw new RuntimeException(e);
        }

    }

    @Override
    public StatusListIndex getIndexFile(String filename) {
        Path filePath = location.resolve(filename).normalize();
        try {
            StatusListIndex index = objectMapper.readValue(filePath.toFile(), StatusListIndex.class);
            return index;
        } catch (IOException e) {
            log.error("Failed to read index from file with name {} due to {}", filename, e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getRevocationList(String filename) {
        Path filePath = location.resolve(filename).normalize();
        try {
            String revocationList = new String(Files.readAllBytes(filePath));
            return revocationList;
        } catch (IOException e) {
            log.error("Failed to read Revocation List from filename: {}", filename);
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<String> getAllRevocationListFilenamesForIssuer(String issuerId, String revocationType) {

        try {
            List<String> filenames = Files.list(location)
                    .filter(path -> {
                        File file = path.toFile();
                        return !file.isDirectory() &&
                                file.getName().contains(issuerId) &&
                                file.getName().contains(revocationType) &&
                                !file.getName().contains("index");
                    })
                    .map( path -> path.toFile().getName())
                    .collect(Collectors.toList());

            log.info("Get all revocation list filenames {}", Arrays.toString(filenames.toArray()));
            return filenames;
        } catch (IOException e) {
            log.error("Failed to read revocation list filenames of Issuer: {} for RevocationType: {} in the system", issuerId, revocationType);
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<String> getAllRevocationListFilenames(String revocationType) {
        try {
            List<String> filenames = Files.list(location)
                    .filter(path -> {
                        File file = path.toFile();
                        return !file.isDirectory() &&
                                file.getName().contains(revocationType) &&
                                !file.getName().contains("index");
                    })
                    .map( path -> path.toFile().getName())
                    .collect(Collectors.toList());

            log.info("Get all revocation list filenames {}", Arrays.toString(filenames.toArray()));
            return filenames;
        } catch (IOException e) {
            log.error("Failed to read revocation list filenames of RevocationType: {} in the system",  revocationType);
            throw new RuntimeException(e);
        }
    }
}
