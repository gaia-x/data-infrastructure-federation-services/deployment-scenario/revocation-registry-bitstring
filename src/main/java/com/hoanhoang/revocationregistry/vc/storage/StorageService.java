package com.hoanhoang.revocationregistry.vc.storage;

import com.hoanhoang.revocationregistry.model.RevocationList;
import com.hoanhoang.revocationregistry.vc.revocation.list.StatusListIndex;
import id.walt.credentials.w3c.VerifiableCredential;
import org.springframework.core.io.Resource;

import java.util.List;

public interface StorageService {

    List<String> getAllRevocationListFilenamesForIssuer(String issuerId, String revocationType);
    List<String> getAllRevocationListFilenames(String revocationType);
    boolean checkIfRevocationListExists(String revocationListUrl);
    void storeIndexFile(String filename, StatusListIndex index);
    StatusListIndex getIndexFile(String filename);
    void storeRevocationList(String filename, String revocationList);
    String getRevocationList(String filename);

}
