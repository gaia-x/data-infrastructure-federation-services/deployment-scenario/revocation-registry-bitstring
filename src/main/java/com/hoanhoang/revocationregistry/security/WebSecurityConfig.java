package com.hoanhoang.revocationregistry.security;


import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
@Slf4j
@SecurityScheme(
        name = "X-API-KEY",
        type = SecuritySchemeType.APIKEY,
        in = SecuritySchemeIn.HEADER
)
public class WebSecurityConfig {

    private String[] URL_WHITE_LIST = {
            "/swagger-ui/**", "/v3/api-docs/**",
            ".well-known/did.json",
            "/api/v1/revocations/verify", "/api/v1/revocations/verify/**",
            "/api/v1/revocations/credentials", "/api/v1/revocations/credentials/**"
    };

    @Value("${registry.security.api-key.header}")
    private String apiKeyRequestHeader;

    @Value("${registry.security.api-key.value}")
    private String apiKeyRequestValue;

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        APIKeyFilter apiKeyFilter = new APIKeyFilter(apiKeyRequestHeader);
        apiKeyFilter.setAuthenticationManager(new AuthenticationManager() {
            @Override
            public Authentication authenticate(Authentication authentication) throws AuthenticationException {
                String principal = (String) authentication.getPrincipal();
                if(! apiKeyRequestValue.equals(principal)){
                    log.error("Failed to authenticate the provided API Key {} with the correct one {}", principal, apiKeyRequestValue );
                    throw new BadCredentialsException("The API Key is not correct");
                } else {
                    log.info("Authenticated successfully");
                }
                authentication.setAuthenticated(true);
                return authentication;
            }
        });

        httpSecurity.cors()
                .and()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilter(apiKeyFilter)
                .authorizeHttpRequests( request -> request
                                .requestMatchers(URL_WHITE_LIST).permitAll()
                                .requestMatchers("/api/v1/revocations/verify").permitAll()
                                .anyRequest().authenticated()
                                //.anyRequest().permitAll()
                );
        return httpSecurity.build();

    }


    @Bean
    public CorsConfigurationSource corsConfigurationSource(){
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowedOriginPatterns(Arrays.asList("*"));
        corsConfiguration.setAllowedHeaders(Arrays.asList("*"));
        corsConfiguration.setAllowedMethods(Arrays.asList("*"));
        corsConfiguration.setAllowCredentials(true);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", corsConfiguration);
        return source;
    }
}
