package com.hoanhoang.revocationregistry.config;

import java.util.List;

public interface Constants {
    int BIT_STRING_LENGTH = 16*1024*8;
    String W3C_VERIFIABLE_CREDENTIAL_CONTEXT = "https://www.w3.org/ns/credentials/v2";
    List<String> BIT_STRING_VC_TYPE = List.of("VerifiableCredential", "BitstringStatusListCredential");
}
