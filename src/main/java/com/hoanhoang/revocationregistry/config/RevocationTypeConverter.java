package com.hoanhoang.revocationregistry.config;

import com.hoanhoang.revocationregistry.enums.RevocationType;
import org.springframework.core.convert.converter.Converter;

public class RevocationTypeConverter implements Converter<String, RevocationType> {

    @Override
    public RevocationType convert(String source) {
        return RevocationType.valueOf(source);
    }
}
