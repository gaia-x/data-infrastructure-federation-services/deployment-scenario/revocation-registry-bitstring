package com.hoanhoang.revocationregistry.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hoanhoang.revocationregistry.vc.revocation.list.RevocationListServiceManager;
import com.hoanhoang.revocationregistry.vc.revocation.service.RevocationManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequiredArgsConstructor
@Slf4j
@Tag(
        name = "DID Web"
)
public class DidController {


    private final RevocationListServiceManager revocationListServiceManager;
    private final ObjectMapper objectMapper;

    @Operation(
            summary = "Get the DID Web of the Revocation Registry",
            description = "Get the DID Web of the Revocation Registry"
    )
    @GetMapping("/.well-known/did.json")
    public ResponseEntity<?> getDIDWeb(){
        String didWeb = revocationListServiceManager.getDIDWebDetails();
        TypeReference<Map<String, Object>> typeReference = new TypeReference<Map<String, Object>>() {};
        try {
            Map<String, Object> didObject = objectMapper.readValue(didWeb, typeReference);

            return ResponseEntity.ok(didObject);
        } catch (JsonProcessingException e) {
            log.error("Failed to read the DID Web Object ");
            throw new RuntimeException(e);
        }

    }
}
