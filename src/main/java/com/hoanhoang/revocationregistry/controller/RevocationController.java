package com.hoanhoang.revocationregistry.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.hoanhoang.revocationregistry.enums.RevocationType;
import com.hoanhoang.revocationregistry.model.RevocationStatusDto;
import com.hoanhoang.revocationregistry.model.RevocationStatusEntry;
import com.hoanhoang.revocationregistry.rest.*;
import com.hoanhoang.revocationregistry.vc.revocation.service.RevocationManager;
import com.hoanhoang.revocationregistry.vc.revocation.service.RevocationProvider;
import com.hoanhoang.revocationregistry.vc.revocation.list.RevocationListHelper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;

@RestController
@Tag(
        name = "Revocation Service"
)
@Slf4j
@RequestMapping("/api/v1/revocations")
@RequiredArgsConstructor
public class RevocationController {

    private final RevocationManager revocationManager;
    private final ObjectMapper objectMapper;

    @Value("${registry.revocation.list.base.url}")
    private String revocationListRegistryBaseUrl;


    private void preValidateRevocationType(RevocationType revocationType){
        if(revocationType != RevocationType.BIT_STRING){
            log.error("The chosen revocation type {} is not yet supported", revocationType.getType());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("The chosen revocation type %s is not yet supported", revocationType.getType()));
        }
    }


    @Operation(
            summary = "Configure a revocation list for an Issuer according to a revocation type",
            description = "**Configure a revocation list for an Issuer according to a revocation type** \n" +
                    "\n\n **size**: Define the number of bits used to represent all states of a status " +
                    "\n\n 1. In case size = 1, the states should be: 0x0 & 0x1" +
                    "\n\n 2. In case size =2, the states should be: 0x00, 0x01, 0x10, & 0x11"
            )
    @SecurityRequirement(
            name = "X-API-KEY"
    )
    @PostMapping("/configure")
    public ResponseEntity<?> configRevocationList(@RequestParam(name = "type", defaultValue = "BIT_STRING") RevocationType revocationType,
                                                  @RequestParam(name = "issuerId", defaultValue = "default") String issuerId,
                                                  @RequestBody @Valid RevocationListParameter revocationListParameter){
        preValidateRevocationType(revocationType);
        if(revocationListParameter.getSize()  <= 0){
            log.error("Failed to configure a revocation list with size: {}", revocationListParameter.getSize());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Failed to configure a revocation list with size: %s", revocationListParameter.getSize()));
        }
        String extendedCredentialUrl = revocationListRegistryBaseUrl +
                "/"
                + RevocationListHelper.createRelativeRevocationListUrl(revocationType, revocationListParameter.getPurpose(), issuerId);

        revocationListParameter.setCredentialUrl(extendedCredentialUrl);
        revocationListParameter.setIssuerId(issuerId);

        RevocationProvider revocationProvider = revocationManager.getRevocationProvider(revocationType);
        revocationProvider.configureRevocationList(revocationListParameter);
        return ResponseEntity.ok().build();
    }



    @Operation(
        summary = "Check if revocation list for an Issuer according to a revocation type has been created",
        description = "**Check if revocation list for an Issuer according to a revocation type has been created** \n" 
        )
        @SecurityRequirement(
        name = "X-API-KEY"
        )
        @GetMapping("/configure")
        public ResponseEntity<?> checkConfigRevocationList(@RequestParam(name = "type", defaultValue = "BIT_STRING") RevocationType revocationType,
                                              @RequestParam(name = "issuerId", defaultValue = "default") String issuerId,
                                              @RequestParam(name = "purpose", defaultValue = "message") String purpose){

        preValidateRevocationType(revocationType);    
        
        RevocationConfigVerificationParameter revocationListConfigParameter = new RevocationConfigVerificationParameter(); 
        revocationListConfigParameter.setPurpose(purpose);                                  
            String extendedCredentialUrl = revocationListRegistryBaseUrl +
            "/"
            + RevocationListHelper.createRelativeRevocationListUrl(revocationType, revocationListConfigParameter.getPurpose(), issuerId);
                        
        revocationListConfigParameter.setCredentialUrl(extendedCredentialUrl);
        revocationListConfigParameter.setIssuerId(issuerId);

        RevocationProvider revocationProvider = revocationManager.getRevocationProvider(revocationType);
        if(revocationProvider.checkRevocationListConfiguration(revocationListConfigParameter)) {
                return ResponseEntity.ok().build();
        } else {
                return ResponseEntity.notFound().build();
        }
}


    @SecurityRequirement(
            name = "X-API-KEY"
    )
    @Operation(
            summary = "Create a credential status entry for a verifiable credential",
            description = "**Create a credential status entry for a verifiable credential given a purpose**" +
                    "\n\n There should be a revocation list, which associates to the purpose, and which has been configured before "
    )
    @PostMapping("/statusEntry")
    public ResponseEntity<?> getRevocationStatusEntry(@RequestParam(name = "type", defaultValue = "BIT_STRING") RevocationType revocationType,
                                                      @RequestParam(name = "issuerId", defaultValue = "default") String issuerId,
                                                      @RequestBody @Valid RevocationStatusRequest request){

        preValidateRevocationType(revocationType);
        String purpose = request.getPurpose();

        RevocationProvider revocationProvider = revocationManager.getRevocationProvider(revocationType);

        String extendedCredentialUrl = revocationListRegistryBaseUrl + "/"+ RevocationListHelper.createRelativeRevocationListUrl(revocationType, purpose, issuerId);
        RevocationStatusDto statusDto = RevocationStatusDto.builder()
                .credentialUrl(extendedCredentialUrl)
                .purpose(purpose)
                .issuerId(issuerId)
                .build();

        List<RevocationStatusEntry> entries = revocationProvider.getRevocationStatusEntry(statusDto);
        return ResponseEntity.ok(entries);
    }



    @Operation(
            summary = "Verify the current status of a verifiable credential",
            description = "**Verify the current status of a verifiable credential given a credential status of the VC**"
    )
    @PostMapping("/verify")
    public ResponseEntity<RevocationStatusResponse> verifyRevocationStatus(@RequestParam(name = "type", defaultValue = "BIT_STRING") RevocationType revocationType,
                                                    @RequestBody @Valid RevocationCheckParameter parameter){

        preValidateRevocationType(revocationType);

        RevocationProvider revocationProvider = revocationManager.getRevocationProvider(revocationType);
        RevocationStatusResponse response  = revocationProvider.verifyRevocationStatus(parameter);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @Operation(
            summary = "Transition the status of a verifiable credential to a specified status",
            description = "**Transition the status of a verifiable credential to a specified status.**" +
                    "\n\n Depending on the revocation list configuration, one MAY or MAY NOT return the *current* state to a *lower level* state" +
                    "\n\n If reversion is not allowed: The transition from **0x11 -> 0x10/0x10/0x00** or from **0X10 -> 0x01/0X00** are prohibited"
    )
    @SecurityRequirement(
            name = "X-API-KEY"
    )
    @PostMapping("/transition")
    public ResponseEntity<?> transitionRevocationStatus(@RequestParam(name = "type", defaultValue = "BIT_STRING") RevocationType revocationType,
                                                        @RequestBody @Valid RevocationChangeRequest changeRequest){

        preValidateRevocationType(revocationType);

        RevocationProvider revocationProvider = revocationManager.getRevocationProvider(revocationType);
        revocationProvider.transitionRevocationStatus(changeRequest.getTargetedStatus(), changeRequest.getRevocationConfigParameter());

        return ResponseEntity.ok().build();
    }


    @Operation(
            summary = "Get all revocation list of an issuer",
            description = "**Get all revocation list that have been created/configured by of an issuer**"
    )
    @GetMapping("/credentials/{issuerId}")
    public ResponseEntity<?> getRevocationListByIssuer(@RequestParam(name = "type", defaultValue = "BIT_STRING") RevocationType revocationType,
            @PathVariable(name = "issuerId") String issuerId){
        RevocationProvider revocationProvider = revocationManager.getRevocationProvider(revocationType);

        List<Map<String, Object>> revocationLists = revocationProvider.getRevocationListByIssuer(issuerId);
        return new ResponseEntity<>(revocationLists, HttpStatus.OK);
    }

    @Operation(
            summary = "Get all revocation lists in the system",
            description = "**Get all revocation lists that have been created/configured in the system**"
    )
    @GetMapping("/credentials")
    public ResponseEntity<?> getAllRevocationList(@RequestParam(name = "type", defaultValue = "BIT_STRING") RevocationType revocationType){
        RevocationProvider revocationProvider = revocationManager.getRevocationProvider(revocationType);

        List<Map<String, Object>> revocationLists = revocationProvider.getAllRevocationList();

        return new ResponseEntity<>(revocationLists, HttpStatus.OK);
    }
}