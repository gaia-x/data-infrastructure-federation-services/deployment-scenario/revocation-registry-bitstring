package com.hoanhoang.revocationregistry.common;

import lombok.extern.slf4j.Slf4j;

import java.util.*;


@Slf4j
public class CommonUtils {

    public static Set<String> generateBinaryStrings(int bits){
        long size = (long) Math.pow(2, bits);
        Set<String> lifOfString = new HashSet<>();
        String prefix= "0x";

        for(int currentValue = 0; currentValue < size; currentValue++){

            String conciseBinaryPres  = Integer.toBinaryString(currentValue);
            int missingSize = bits - conciseBinaryPres.length();
            String missingString = "";
            if(missingSize > 0){
                // Creating a string filled with 'missingSize' "0"
                char[] missingChars = new char[missingSize];
                Arrays.fill(missingChars, '0');
                missingString = new String(missingChars);
            }
            lifOfString.add(prefix + missingString + conciseBinaryPres);
        }
        log.info("Get the list of string : {}", Arrays.toString(lifOfString.toArray()));
        return lifOfString;
    }
}
