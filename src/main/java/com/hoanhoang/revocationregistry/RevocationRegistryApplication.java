package com.hoanhoang.revocationregistry;

import id.walt.servicematrix.ServiceMatrix;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(
        info = @Info(
                title = "Revocation Registry",
                description = "Revocation Registry"
        )
)
public class RevocationRegistryApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(RevocationRegistryApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        new ServiceMatrix("service-matrix.properties");
    }
}
