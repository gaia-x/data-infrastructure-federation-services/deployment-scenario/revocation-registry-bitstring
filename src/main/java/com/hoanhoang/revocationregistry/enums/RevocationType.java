package com.hoanhoang.revocationregistry.enums;


import lombok.Getter;

@Getter
public enum RevocationType {

    BIT_STRING("bit-string"),
    OAUTH_STATUS_LIST("oauth-status-list");

    String type;

    RevocationType(String type){
        this.type = type;
    }
}
