package com.hoanhoang.revocationregistry.enums;

public enum RevocationStatusDefinition {
    SUSPENDED,
    REVOKED,
    ACTIVE;
}
