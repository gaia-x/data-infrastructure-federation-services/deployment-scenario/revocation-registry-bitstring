FROM maven:3.8.5-openjdk-17 as build

WORKDIR /opt/app
COPY ./config ./
COPY pom.xml .
COPY ./src ./src

RUN mvn clean install 



FROM maven:3.8.5-openjdk-17
WORKDIR /app
COPY ./config ./
COPY --from=build /opt/app/target/revocation-registry-v1.0.jar /app/revocation-registry-v1.0.jar

ENTRYPOINT ["java", "-jar", "revocation-registry-v1.0.jar"]
