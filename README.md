## Revocation Registry

### Description

This component implements the lastest lifecycle mecanism of the W3C Verifiable Credential, the Bitstring Status List (link to the specificatoin)[https://www.w3.org/TR/vc-bitstring-status-list/]

The Bitstring Status list allows Issuer to manage the lifecycle of the Verifiable Credential they are issuing by exposing various list and then referencing them in the fied `CredentialStatus`.

Using this component, an Issuer can issue a list of type:

- Revocation: A list that allow to attribute a binary state to a credential (either active or revoked). A revoked credential cannot gain the status Active

- Suspention: A list that allow to attribute a binary state to a credential (either active or suspended). A suspended credential can gain the status Active again through a reactivation

- Message: A list that was introduced with the Bitstring Status List. A list of type message allow an Issuer to create a list that can have 2^n type of message (depending on the bitsize of each entry). It means that implementors can create list of both suspention and revocation within the same list, as well as create much more detailed status


## Use the component

## Creating a list

To create a status list, an issuer needs to use the route /api/v1/revocations/configure with a POST Request. It is not possible to create an entry in a non existing list, so it is also possible to use the /api/v1/revocations/configure route with a GET request to check whether the list has been initiated

### Body and parameter of the request 

To configure a list, you will need to set two parameters to your request

- type: an Enum where you should select BIT_STRING

- issuerId: In this component, there is only one revocation registry in all Aster-X ecosystem to avoid repetitive deployment and thus it is needed to have a way to index list depending on the issuer


Implementors should also join a body to the request such as : 
```json
{
  "purpose": "string",
  "reference": "string",
  "ttl": 0,
  "statusMessages": [
    {
      "status": "string",
      "value": "string"
    }
  ],
  "size": 0,
  "reversible": true
}
```

in this body: 
- purpose: The type of the list, it can be suspension, revocation or message
- reference: an index for your status list
- ttl: the time to live for your list
- size: a bit size for your index (size of 1 = 2 message, 2=4 messages, ...)
- reversible: whether you want to be able to go from a higher status (in term of bit value ) to a lower one 
- statusMessage: an array to map each bit to a message (must have a number element to match your bitsize)


## Get an entry

You can use the route /api/v1/revocations/statusEntry as a post Request to get a Status Entry of your chosen list

### Body and parameter of the request 

To get a Status entry, you will need to add as parameter: 

- type: an Enum where you should select BIT_STRING

- issuerId: In this component, there is only one revocation registry in all Aster-X ecosystem to avoid repetitive deployment and thus it is needed to have a way to index list depending on the issuer

you will also need to provide a purpose as body (revocation, suspension, message) 



## Modify a status 


To modify a Status, you can use the route /api/v1/revocation/transition using BIT_STRING as type parameter

### Body

You will also need to provide a json body such as : 

```json 
{
  "targetedStatus": "string",
  "credentialStatus": {
    "type": "string",
    "id": "string",
    "statusPurpose": "string",
    "statusListIndex": "string",
    "statusListCredential": "string"
  }
}
```

Here, targetedStatus represent the bit you want to transition to (such as 0x10) and credential Status the entry stored in the credential you issued

### Build and Test
To locally run the project, you can build a docker image then run a container using the image OR alternatively use directly the docker-compose file for convenience:

1. Run the project with docker-compose:
```
docker-compose up 
```

2. Build an revocation registry image:
```javascript
docker build -t revocation-registry:v1.0
```
Then launch a container using the resulting image:

```
docker run -it -p 8181:8181 --name registry-container revocation-registry:v1.0
```

a swagger is avalaible at the route /swagger-ui/index.html